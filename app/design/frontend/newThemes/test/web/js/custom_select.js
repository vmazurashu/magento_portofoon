define([
        "jquery",
        'jquery/ui',
        'uiComponent'
    ],
    function($) {
        "use strict";

        $(document).ready(function(){
            $(".sorter-label").click(function(){
                $("#sorter").toggle();
            });
        });

        $(function() {
            $("#sorter").attr('size', '3');
        });

        $(".toolbar-sorter.sorter").append("<span class='icon-down'></span>");
    });
